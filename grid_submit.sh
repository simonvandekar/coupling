#!/bin/bash

fwhm=5
scripts=/import/speedy/eons/progs/freesurfer/lwr
#sublist=$scripts/bblid_scanid.txt
sublist=$scripts/lwr/subject_list.txt
logdir=$scripts/logdir
mkdir $logdir 2>/dev/null
rm -f $logdir/*
queue="free.q"
time=$(date | cut -d " " -f4 | sed "s+:++g")


nsub=$(cat $sublist | wc -l )
qsub -V -N coupling_$time -t 1 -cwd -o $logdir -e $logdir -q $queue $scripts/lwr/coupling_v1.R -s $sublist -f $fwhm -o
qsub -V -t 2-$nsub -cwd -hold_jid coupling_$time -o $logdir -e $logdir -q $queue $scripts/lwr/coupling_v1.R -s $sublist -f $fwhm -o
