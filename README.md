#### DISCLAIMER ####
Copyright (c) 2016, Simon Vandekar, UNIVERSITY OF PENNSYLVANIA
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the University of Pennsylvania.

#### DESCRIPTION ####
Coupling is a localized estimate of the spatial relationship between two cortical measures.

#### SETUP ####
These scripts will require your FREESURFER_HOME environment variable to 
be set.
There are two options for setup:
1) Run the setup script which will require sudo to add files to the freesurfer
home folder
2) Perform all the steps in the setup script manually.

Option 1):
First cd to the folder where you downloaded the coupling scripts and run
setup.R:
/path/to/preferred/R/version/bin/R --slave --file=./setup.R
This script will try to download a required R package "optparse" and create a
folder in your freesurfer home directory called "R" where it will place the
code required to run coupling.
It will ask for sudo permissions when making $FREESURFER_HOME/R and copying the R
code into the directory, but not when installing the package.
It will make this version of R the default version used to estimate coupling.
There is a string in the top of the coupling script "###SHEBANG###" That gets replaced
by "/path/to/preferred/R/version/bin/Rscript", so that the script will run at the 
command line.

Option 2):
Perform the steps above. You will need appropriate permissions for each step.

After running the setup script, add $FREESURFER_HOME/R to your path by adding
this line to your .bashrc:
export PATH=$PATH:$FREESURFER_HOME/R
The coupling_v*.R script also require that your SUBJECTS_DIR is set.
You will also need Rscript in your version of R e.g.
/path/to/preferred/R/version/bin/Rscript

#### FILES ####
Files included in this package:
coupling_v*.R is a command line R program that estimates coupling for a given
list of subjects. type coupling_v1.R -h for help

kth_neighbors_v*.R is run by coupling_v*.R and estimates the first k sets of
nearest neighbors for each vertex for a particular template. This takes the
longest amount of time, but only needs to be run once. This can be modified
with the maxneigh argument in coupling_v*.R.

grid_submit.sh is an example on how to run coupling_v*.R on Sun Grid Engine.
Coupling automatically tries to detect SGE, or LSF array id variables. If it
cannot find either of them then it runs sequentially. grid_submit.sh waits for
the first job to run successfully before trying to run the full list of subjects.
The first subject run finds the kth nearest neightbors for the given template.
All other jobs will wait for the successful completion of the first subject
before running.

#### EXAMPLES ####
echo $SUBJECTS_DIR # $FREESURFER_HOME/subjects
cat my_subject_list.txt # bert
coupling_v1.R -s ./my_subject_list.txt
coupling_v1.R -s ./my_subject_list.txt -m "thickness,curv" # with curvature instead of sulcal depth
coupling_v1.R -s ./my_subject_list.txt -m "sulc,thickness" # reverse of default relationship
coupling_v1.R -s ./my_subject_list.txt -f 3 # less weighting than default
coupling_v1.R -s ./my_subject_list.txt -t fsaverage4 # coarser template. (gives smoother result due to weighting being in neighbor units)
coupling_v1.R -s ./my_subject_list.txt -t fsaverage6 # finer template (gives less smooth result).
coupling_v1.R -s ./my_subject_list.txt -o # overwrites files
coupling_v1.R -s ./my_subject_list.txt -n 15 # max number of neighborly separation with nonzero weight. Not really essential, 10 (default) works well in practice


#### OUTPUT ####
Neighbor information is stored here in an R rds file for this template.
$SUBJECTS_DIR/$template/surf/[lr]h.*neighbors_degree.rds

If they don't already exists, labels are created for this template using mri_annotation2label in
$SUBJECTS_DIR/$template/label

In each subjects directory:
$SUBJECTS_DIR/bert/surf/[lr]h.coupling_${measure1}_${measure2}.fwhm${fwhm}.$template
$SUBJECTS_DIR/bert/surf/[lr]h.coupling_${measure1}_${measure2}.fwhm${fwhm}.${template}.asc
also intermediate files:
$SUBJECTS_DIR/bert/surf/[lr]h.{$measure1,$measure2}.$template

#### COMMENTS ####
files like
$SUBJECTS_DIR/bert/surf/[lr]h.coupling_${measure1}_${measure2}.fwhm${fwhm}.$template
can be analyzed as you would typically with other surfaces, e.g. using mri_glmfit or
another software.
